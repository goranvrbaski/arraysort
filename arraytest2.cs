using System;
using System.Diagnostics;
 
 
namespace ArrayTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int length = 0;
 
            Console.WriteLine("Please enter length of array to test: ");
            try {
                length = Convert.ToInt32(Console.ReadLine());
            } catch {
                // some exception
            }
 
            // generate empty array of "length" elements
            int[] arr = new int[length];
 
            // generate random numbers and populate array
            Random rndNumber = new Random(length);
            for (int i = 0; i < length; i++) { 
                arr[i] = rndNumber.Next();
            }
 
 
            Stopwatch time = new Stopwatch();
 
            // bubble sort test
            time.Start();
            int[] bubbleSorted = bubbleSort(arr);
            time.Stop();
 
            // store test time and reset time
            long bubbleTime = time.ElapsedMilliseconds;
            time.Reset();
 
            // quick sort test
            time.Start();
            int[] quickSorted = IntArrayQuickSort(arr);
            time.Stop();
            long quickTime = time.ElapsedMilliseconds;
 
            // print first 10 elements of array(s)
            if (length > 10)
            {
                Console.WriteLine("\n\nPrinting only first 10 sorted items...\n");
                for (int i = 0; i < 10; i++)
                {
                    Console.Write("{0} ", bubbleSorted[i]);
                }
 
                Console.WriteLine("...");
 
                for (int i = 0; i < 10; i++)
                {
                    Console.Write("{0} ", quickSorted[i]);
                }
 
                Console.WriteLine("...");
            }
            else {
                Console.WriteLine();
                for (int i = 0; i < length; i++)
                {
                    Console.Write("{0} ", bubbleSorted[i]);
                }
 
                Console.WriteLine("\n");
 
                for (int i = 0; i < length; i++)
                {
                    Console.Write("{0} ", quickSorted[i]);
                }
            }
 
            // write and read log file
            writeToLog(length, bubbleTime, time.ElapsedMilliseconds);
            string text = System.IO.File.ReadAllText(@"results.txt");
            Console.WriteLine();
            Console.WriteLine("\n{0}", text);
 
            // wait user action to exit
            Console.ReadKey();
 
 
        }
 
        private static int[] bubbleSort(int[] arr)
        {
            int temp = 0;
            for (int write = 0; write < arr.Length; write++)
            {
                for (int sort = 0; sort < arr.Length - 1; sort++)
                {
                    if (arr[sort] > arr[sort + 1])
                    {
                        temp = arr[sort + 1];
                        arr[sort + 1] = arr[sort];
                        arr[sort] = temp;
                    }
                }
            }
 
            return arr;
        }
 
        public static int[] IntArrayQuickSort(int[] data, int l, int r)
        {
            int i, j;
            int x;
 
            i = l;
            j = r;
 
            x = data[(l + r) / 2]; /* find pivot item */
            while (true)
            {
                while (data[i] < x)
                    i++;
                while (x < data[j])
                    j--;
                if (i <= j)
                {
                    exchange(data, i, j);
                    i++;
                    j--;
                }
                if (i > j)
                    break;
            }
            if (l < j)
                IntArrayQuickSort(data, l, j);
            if (i < r)
                IntArrayQuickSort(data, i, r);
            return data;
        }
 
        public static int[] IntArrayQuickSort(int[] data)
        {
            return IntArrayQuickSort(data, 0, data.Length - 1);
        }
 
        public static void exchange(int[] data, int m, int n)
        {
            int temporary;
 
            temporary = data[m];
            data[m] = data[n];
            data[n] = temporary;
        }
 
        private static void writeToLog(int length, long time, long qtime) {
            System.IO.StreamWriter file = new System.IO.StreamWriter(@"results.txt");
            file.WriteLine("--------- BUBBLE SORT TEST ---------");
            file.WriteLine("Array length: {0}\nTime: {1} ms", length, time);
            file.WriteLine("--------------- END ----------------");
            file.WriteLine();
            file.WriteLine("--------- QUICK SORT TEST ----------");
            file.WriteLine("Array length: {0}\nTime: {1} ms", length, qtime);
            file.WriteLine("--------------- END ----------------");
            file.Close();
        }
    }
}