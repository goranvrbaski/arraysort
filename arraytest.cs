using System;
using System.Diagnostics;


namespace ArraySort
{
    class Program
    {
        static void Main(string[] args)
        {
            int length = 0;

            Console.WriteLine("Please enter length of array to test: ");
            try
            {
                length = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                // some exception
            }

            // generate empty array of "length" elements
            int[] arr = new int[length];

            // generate random numbers and populate array
            Random rndNumber = new Random(length);
            for (int i = 0; i < length; i++)
            {
                arr[i] = rndNumber.Next();
            }


            Stopwatch time = new Stopwatch();

            // bubble sort test
            time.Start();
            int[] bubbleSorted = bubbleSort(arr);
            time.Stop();

            // store test time and reset time
            long bubbleTime = time.ElapsedMilliseconds;
            time.Reset();

            // quick sort test
            time.Start();
            int[] quickSorted = IntArrayQuickSort(arr);
            time.Stop();
            long quickTime = time.ElapsedMilliseconds;
            time.Reset();

            // merge sort test
            time.Start();
            int[] mergeSorted = mergeSort(arr);
            time.Stop();
            long mergeTime = time.ElapsedMilliseconds;
            time.Reset();

            // naive sort test
            time.Start();
            int[] naiveSorted = naiveSort(arr);
            time.Stop();
            long naiveTime = time.ElapsedMilliseconds;
            time.Reset();

            // insertion sort test
            time.Start();
            int[] insertionSorted = naiveSort(arr);
            time.Stop();
            long insertionTime = time.ElapsedMilliseconds;
            time.Reset();

            // print first 10 elements of array(s)
            if (length > 10)
            {
                Console.WriteLine("\n\nPrinting only first 10 sorted items...\n");
                for (int i = 0; i < 10; i++)
                {
                    Console.Write("{0} ", bubbleSorted[i]);
                }

                Console.WriteLine("...");

                for (int i = 0; i < 10; i++)
                {
                    Console.Write("{0} ", quickSorted[i]);
                }

                Console.WriteLine("...");


                for (int i = 0; i < 10; i++)
                {
                    Console.Write("{0} ", mergeSorted[i]);
                }

                Console.WriteLine("...");

                for (int i = 0; i < 10; i++)
                {
                    Console.Write("{0} ", naiveSorted[i]);
                }

                Console.WriteLine("...");

                for (int i = 0; i < 10; i++)
                {
                    Console.Write("{0} ", insertionSorted[i]);
                }

                Console.WriteLine("...");
            }
            else
            {
                Console.WriteLine();
                for (int i = 0; i < length; i++)
                {
                    Console.Write("{0} ", bubbleSorted[i]);
                }

                Console.WriteLine("\n");

                for (int i = 0; i < length; i++)
                {
                    Console.Write("{0} ", quickSorted[i]);
                }


                for (int i = 0; i < length; i++)
                {
                    Console.Write("{0} ", mergeSorted[i]);
                }

                for (int i = 0; i < length; i++)
                {
                    Console.Write("{0} ", naiveSorted[i]);
                }

                for (int i = 0; i < length; i++)
                {
                    Console.Write("{0} ", insertionSorted[i]);
                }
            }

            // write and read log file
            writeToLog(length, bubbleTime, quickTime, mergeTime, naiveTime, insertionTime);
            string text = System.IO.File.ReadAllText(@"results.txt");
            Console.WriteLine();
            Console.WriteLine("\n{0}", text);

            // wait user action to exit
            Console.ReadKey();


        }

        private static int[] bubbleSort(int[] arr)
        {
            int temp = 0;
            for (int write = 0; write < arr.Length; write++)
            {
                for (int sort = 0; sort < arr.Length - 1; sort++)
                {
                    if (arr[sort] > arr[sort + 1])
                    {
                        temp = arr[sort + 1];
                        arr[sort + 1] = arr[sort];
                        arr[sort] = temp;
                    }
                }
            }

            return arr;
        }

        private static int[] IntArrayQuickSort(int[] data, int l, int r)
        {
            int i, j;
            int x;

            i = l;
            j = r;

            x = data[(l + r) / 2];
            while (true)
            {
                while (data[i] < x)
                    i++;
                while (x < data[j])
                    j--;
                if (i <= j)
                {
                    exchange(data, i, j);
                    i++;
                    j--;
                }
                if (i > j)
                    break;
            }
            if (l < j)
                IntArrayQuickSort(data, l, j);
            if (i < r)
                IntArrayQuickSort(data, i, r);
            return data;
        }

        private static int[] IntArrayQuickSort(int[] data)
        {
            return IntArrayQuickSort(data, 0, data.Length - 1);
        }

        private static void exchange(int[] data, int m, int n)
        {
            int temporary;

            temporary = data[m];
            data[m] = data[n];
            data[n] = temporary;
        }

        private static int[] naiveSort(int[] array) {
            for (int i = 0; i < array.Length; i++)
            {
                for(int j = 1; j < array.Length; j++)
                {
                    if(array[i] > array[j])
                        exchange(array, i, j);
                }
            }

            return array;
        }

        private static int[] mergeSort(int[] array) {
            if (array.Length > 1) {
                int sredina = array.Length / 2;
                int[] levo = new int[sredina];
                int[] desno = new int[sredina];


                for (int i = 0; i < sredina; i++) {
                    levo[i] = array[i];
                }

                for (int i = 1; i < sredina; i++)
                {
                    desno[i] = array[sredina + 1];
                }



                mergeSort(levo);
                mergeSort(desno);

                int m = 0, j = 0, k = 0;
                while(m<levo.Length && j<desno.Length) {
                    if (levo[m] < desno[m])
                    {
                        array[k] = levo[m];
                        m++;
                    }
                    else {
                        array[k] = desno[j];
                        j++;
                    }
                }

                while (m < levo.Length)
                {
                    array[k] = levo[m];
                    m++;
                    k++;
                }

                while (m < desno.Length) {
                    array[k] = desno[j];
                    j++;
                    k++;
                }
            }

            return array;
        }

        private static int[] insertionSort(int[] array) {
            for (int i = 0; i < array.Length - 1; i++) {
                int x = array[i];
                int j = i - 1;
                while (j >= 0 && array[j] > x) {
                    array[j + 1] = array[j];
                    j--;
                }
                array[j + 1] = x;
            }

                return array;
        }

        private static void writeToLog(int length, long time, long qtime, long mtime, long ntime, long itime)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(@"results.txt");
            file.WriteLine("--------- BUBBLE SORT TEST ---------");
            file.WriteLine("Array length: {0}\nTime: {1} ms", length, time);
            file.WriteLine("--------------- END ----------------");
            file.WriteLine();
            file.WriteLine("--------- QUICK SORT TEST ----------");
            file.WriteLine("Array length: {0}\nTime: {1} ms", length, qtime);
            file.WriteLine("--------------- END ----------------");
            file.WriteLine();
            file.WriteLine("--------- MERGE SORT TEST ----------");
            file.WriteLine("Array length: {0}\nTime: {1} ms", length, mtime);
            file.WriteLine("--------------- END ----------------");
            file.WriteLine();
            file.WriteLine("--------- NAIVE SORT TEST ----------");
            file.WriteLine("Array length: {0}\nTime: {1} ms", length, ntime);
            file.WriteLine("--------------- END ----------------");
            file.WriteLine();
            file.WriteLine("--------- INSERTION SORT TEST ----------");
            file.WriteLine("Array length: {0}\nTime: {1} ms", length, itime);
            file.WriteLine("--------------- END ----------------");
            file.Close();
        }
    }
}